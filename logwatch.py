#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from os import path
from pprint import pprint, pformat
from phabricator import Phabricator
import configparser


"""
Regexes
"""

# Full log line
lineformat = re.compile(
    r"(?P<ipaddress>[0-9.]+|[0-9a-fA-F:]+) - - "  # lazy IP matching
    r"\[(?P<datetime>\d{2}\/[A-z]{3}\/\d{4}:\d{2}:\d{2}:\d{2} (\+|\-)\d{4})\] "
    r"\"(?P<method>GET|POST|DELETE|HEAD|OPTIONS|PATCH|PUT) "
    r"(?P<pagepath>.*) HTTP/[1-2].[0-9]\" "
    r"(?P<returncode>\d{3}) (?P<pagesize>\d+) \"(?P<useragent>.*)\"",
    re.IGNORECASE,
)

# dates at the start of blog articles path
urldate = re.compile(r"^\/\d{4}\/\d{2}\/\d{2}")

# "feed" mention at the end of RSS path
feed = re.compile(r"\/feed\/$")

"""
Methods
"""


def parseline(data):
    """
    For now, looking only at GET calls to blog articles that are too short
    and ignoring RSS feeds
    """
    if data["method"] == "GET" and re.search(urldate, data["pagepath"]):
        ps = int(config["pagesize"])
        if int(data["pagesize"]) < ps and data["returncode"] == "200":
            if not re.search(feed, data["pagepath"]):
                alerts.append(data)


def readfile(logfile):
    for l in logfile:
        data = re.search(lineformat, l)
        if data:
            parseline(data.groupdict())
        else:
            pass
            # Ignoring bad requests
            # print("[ERROR] Couldn't read line: {}".format(l))


if __name__ == "__main__":
    # Get the server config
    configfile = configparser.ConfigParser()
    configfile.read(path.join(path.dirname(__file__), "config.ini"))
    config = {}
    config["logdir"] = configfile.get("nginx", "logdir")
    config["logfilename"] = configfile.get("nginx", "logfilename")
    config["pagesize"] = configfile.get("nginx", "pagesize")
    config["ticket"] = configfile.get("phabricator", "ticket")

    alerts = []

    with open(path.join(config["logdir"], config["logfilename"])) as logfile:
        readfile(logfile)

    if len(alerts):
        phab = Phabricator()
        comment = "Billet de blog renvoyé avec moins de {} octets :\n".format(
            config["pagesize"])
        for a in alerts:
            comment += "```"
            comment += pformat(a, indent=4)
            comment += "\n```\n"
        # Reopening the task if closed and adding a comment
        task_details = [
            {"type": "status", "value": "open"},
            {"type": "comment", "value": comment},
        ]

        ret = phab.maniphest.edit(
            transactions=task_details,
            objectIdentifier=config["ticket"])
        if ret:
            pprint("Phabricator task updated.")

    else:
        pprint("No alert to report")
