# website-logwatch

Watch the logs of the website and open ticket if needed.

## Installation

- `git clone https://framagit.org/wikimedia-france/website-logwatch.git`
- Copy config-sample.ini to config.ini and set the correct values
- Copy .arcrc to ~/.arcrc and set the correct values
- pip install -r requirements.txt